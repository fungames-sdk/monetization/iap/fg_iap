# IAP

## Introduction

In App Purchases are used to sell in-app products within your game. To make it work you will first need to create products in your **App Store** or **Google Play** app page.  

To setup your products in your Unity project you will first need to create the **FGIAPSettings asset** in the _Resource/FunGames_ folder. To do so click _**Create > FunGames > FGIAPSettings**_ and add your products to the list.

To create a product object, click _**Create > FunGames > IAP > FGProduct**_

## Integration Steps

1) **"Install"** or **"Upload"** FG IAP plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Scripting API

> You can subscribe to these callbacks to o attach custom actions to IAP events :

```csharp
FGIAP.Callbacks.Initialization;
FGIAP.Callbacks.OnInitialized;
FGIAP.Callbacks.OnPurchaseRequested;
FGIAP.Callbacks.OnPurchaseSucceeded;
FGIAP.Callbacks.OnPurchaseFailed;
FGIAP.Callbacks.OnRestorationSucceeded;
FGIAP.Callbacks.OnRestorationFailed;
```
> Use the BuyProduct method as shown in the example below to request a purchase for a given product:

```csharp
FGIAP.BuyProduct("com.myApp.myProduct");
```

> Use GetProductInfo to get information from a given product

```csharp
FGIAP.GetProductInfo("com.myApp.myProduct");
```

> In iOS, attach the Restore method to a button to manually request restore for all products:

```csharp
FGIAP.Restore();
```

> See example below to grant a user with the product he purchased:

```csharp
   private void Start()
    {
        FGIAP.Callbacks.OnPurchaseSucceeded += ProductPurchased;
    }

    private void ProductPurchased(FGIAPProduct product)
    {
        if ("com.myApp.myProduct".Equals(product.Id))
        {
            // myProduct purchased ! Make sure to prevent from duplicating rewards.
        }
    }
```